from django import forms

from meetup.models import Meetup


class MeetupForm(forms.ModelForm):

    class Meta:
        model = Meetup
        exclude = ['active']
