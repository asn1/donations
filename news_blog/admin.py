from django.contrib import admin

# Register your models here.
from .models import NewsPost, ContactMessage

class NewsPostAdmin(admin.ModelAdmin):
	list_display = ['title', 'updated', 'when']
	search_fields = ['title', 'content']
	class Meta:
		model = NewsPost

admin.site.register(NewsPost)

admin.site.register(ContactMessage)