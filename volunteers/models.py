from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from volunteer_positions.models import Position

# Create your models here.

def upload_location(instance, filename):
	return "%s/%s" %(instance.id, filename)


class Volunteer(models.Model):
	name = models.CharField(max_length=250)
	nitch = models.ForeignKey(Position, blank=True, null=True)
	detail = models.TextField(max_length=100)
	picture = models.ImageField(
		upload_to=upload_location,
		null=True, blank=True,
		width_field="width_field",
		height_field="height_field")
	height_field = models.IntegerField(default=0)
	width_field = models.IntegerField(default=0)
	when = models.DateTimeField(auto_now=False, auto_now_add=True)
	updated = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __str__(self):
		return self.name

	class Meta:
		ordering = ["-when", "-updated"]

