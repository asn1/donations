from django.contrib import admin

# Register your models here.
from .models import Volunteer


class VolunteerAdmin(admin.ModelAdmin):
	list_display = ['name', 'nitch', 'updated', 'when']
	class Meta:
		model = Volunteer


admin.site.register(Volunteer)