"""donations URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
# from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views
# from news_blog.views import contact
# from meetup import views as meetup_views
from accounts import views as accounts_views
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^about/$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^vision/mission/$', TemplateView.as_view(template_name='account/vision_mission.html'), name='vision_mission'),
    # url(r'^meetups/$', meetup_views.meetup_list, name='meetups'),
    url(r'^donate/$', accounts_views.make_gift, name='make_gift'),
    url(r'^bot/$', TemplateView.as_view(template_name='bot.html'), name='bot'),
    url(r'^thanks/$', TemplateView.as_view(template_name='thanks.html'), name='thanks'),
    url(r'^thanks_donor/$', TemplateView.as_view(template_name='thanks_donation.html'), name='thanks_donation'),
    url(r'^thanks_error/$', TemplateView.as_view(template_name='thanks_error.html'), name='thanks_error'),
    # url(r'^contact/$', contact, name='contact'),
    url(r'^mission/$', TemplateView.as_view(template_name='vision.html'), name='mission'),
    url(r'^future/$', TemplateView.as_view(template_name='future.html'), name='future'),
    url(r'^executives/$', TemplateView.as_view(template_name='executives.html'), name='executives'),
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    # url(r'^', include("charities.urls")),
    url(r'^accounts/', include("accounts.urls")),
    url(r'^volunteer/', include("volunteer.urls")),
    url(r'^event/', include("event.urls")),
    url(r'^faq/', include("faq.urls")),
    url(r'^news/', include("news.urls")),
    url(r'^contact/', include("contact.urls")),
    url(r'^convention/', include("convention.urls")),
    url(r'^payment/', include("payment.urls")),
]

urlpatterns += [
    url(r'accounts/login/$',
        views.login,
        {'template_name': 'account/login.html'},
        name='login'),
    url(r'accounts/logout/$',
        views.logout,
        {'next_page': '/'},
        name='logout'),
]

admin.site.site_header = 'Atheist Society of Nigeria Admin'


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
